import os
import requests
import json
import pickle

categories = ["Arcanes.json",
              "Arch-Gun.json",
              "Arch-Melee.json",
              "Archwing.json",
              "Enemy.json",
              "Fish.json",
              "Gear.json",
              "Glyphs.json",
              "Melee.json",
              "Misc.json",
              "Mods.json",
              "Node.json",
              "Pets.json",
              "Primary.json",
              "Quests.json",
              "Relics.json",
              "Resources.json",
              "Secondary.json",
              "SentinelWeapons.json",
              "Sentinels.json",
              "Sigils.json",
              "Skins.json",
              "Warframes.json"]


def refresh(only_index):
    # fetch item info from GitHub:
    if not only_index:
        for category in categories:
            print(f"fetching {category}")
            json = requests.get(
                "https://raw.githubusercontent.com/WFCD/warframe-items/development/data/json/" + category)
            filename = 'json_data' + os.sep + category
            os.makedirs(os.path.dirname(filename), exist_ok=True)
            print(f"writing {filename}")
            open(filename, 'wb').write(json.content)

    index()


def index():
    print("---Starting indexing---")
    for category in categories:
        print(f"Indexing {category}")
        filename = 'json_data' + os.sep + category
        with open(filename, "r") as file:
            category_info = json.load(file)
        itemnames = []
        for item in category_info:
            itemnames.append(item["name"])
        index_filename = ('index_data' + os.sep + category).replace(".json", "")
        os.makedirs(os.path.dirname(index_filename), exist_ok=True)
        print(f"Writing {index_filename}")
        with open(index_filename, "wb") as index_file:
            pickle.dump(itemnames, index_file)

        # Now index all the items data …
        data_index_filename = ('index_data' + os.sep + 'data' + os.sep + category).replace(".json", "")
        os.makedirs(os.path.dirname(data_index_filename), exist_ok=True)
        print(f"Writing {index_filename}")
        item_data = []
        for item in category_info:
            item_data.append(item)
        with open(data_index_filename, "wb") as data_index_file:
            pickle.dump(item_data, data_index_file)


def collect_dict_info(info: list, src: dict, initial: str):
    for key, val in src.items():
        info.append(initial + " " + str(key))
        if isinstance(val, dict):
            info = collect_dict_info(info, val, str(key))
    return info


def load_keys(category: str):
    filename = 'json_data' + os.sep + category
    with open(filename, "r") as file:
        category_info = json.load(file)
    return category_info[0].keys()


def load_full_index():
    index_strings = []
    for category in categories:
        print(f"loading category {category} to index …")
        filename = ('index_data' + os.sep + 'data' + os.sep + category).replace(".json", "")
        with open(filename, "rb") as file:
            category_info = pickle.load(file)
            for item in category_info:
                # print(item)
                index_strings = collect_dict_info(index_strings, item, item["name"])
    return index_strings


class IndexStore:
    def __init__(self):
        self.index = load_full_index()

    def get_index(self):
        return self.index

    def get_tree_store(self):
        filename = ('index_data' + os.sep + 'treestore')
        try:
            with open(filename, "rb") as file:
                treestore = pickle.load(file)
                return treestore
        except FileNotFoundError:
            return None

    def set_tree_store(self, treestore):
        filename = ('index_data' + os.sep + 'treestore')
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "wb") as file:
            pickle.dump(treestore, file)
