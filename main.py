import sys
import threading

import gi
import cache
import search
from settings import AppSettingsWindow
from progress_dialog import ProgressDialogWindow

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gio, GLib


class AppMainWindow(Gtk.ApplicationWindow):
    def __init__(self, application):
        self.last_results = None
        self.Application = application
        try:
            self.GtkBuilder = Gtk.Builder.new_from_file("window.ui")
            self.GtkBuilder.connect_signals(self)
        except GObject.GError:
            print("Error reading UI file")
            raise

        self.index_store = cache.IndexStore()

        self.se_default = self.GtkBuilder.get_object("se_default")
        # self.entrycompetion: Gtk.EntryCompletion = self.GtkBuilder.get_object("entrycompletion")
        self.completion = Gtk.EntryCompletion()
        self.completion.set_inline_completion(True)
        self.completion.set_minimum_key_length(1)
        self.completion.set_text_column(0)
        self.se_default.set_completion(self.completion)
        self.list_store = Gtk.ListStore(str)
        self.completion.set_model(None)

        self.results_tree = self.GtkBuilder.get_object("results_tree")
        self.grid_results = self.GtkBuilder.get_object("grid_results")
        self.listbox_results = self.GtkBuilder.get_object("listbox_results")

        self.window = self.GtkBuilder.get_object("window")
        self.window.set_application(application)

        self.window.set_keep_above(True)
        display = self.window.get_display()

        # self.window.set_decorated(False)
        monitor = display.get_primary_monitor()
        geometry = monitor.get_geometry()
        width = geometry.width

        height = geometry.height
        print(f"monitor has a width of {width} and a hight of {height} - so let's palce the window at {height / 2}.")
        self.window.move(0, height / 2)
        self.window.show_all()

    def set_treestore(self, treestore):
        # self.se_default.thaw_child_notify()
        self.list_store = treestore

    def on_se_default_search_changed(self, entry: Gtk.SearchEntry):
        if " " in entry.get_text():
            self.update_suggestions(entry.get_text())
        else:
            self.update_suggestions(entry.get_text())
        # self.entrycompetion.complete()

    def update_suggestions(self, prefix):
        suggestions = search.get_completitions(prefix)
        if suggestions is not None:
            self.list_store.clear()
            for completion in suggestions:
                # print(f"adding completion {count}/{len(self.completions)}")
                # print([str(completion)])
                # self.treestore.insert(None, -1, [str(completion)])
                print([str(completion)])
                self.list_store.append([str(completion)])
            self.completion.set_model(self.list_store)

    def on_btn_refresh_clicked(self, button):
        cache.refresh(True)

    def on_button_settings_clicked(self, button):
        win = AppSettingsWindow()

    def on_btn_search_clicked(self, button):
        # print(search.test())
        results = search.do_search(self.se_default.get_text())
        print(f"Results: {results}")
        # print(search.do_search(self.se_default.get_text()))
        self.display_results(results)
        self.last_results = results

    def on_btn_quit_clicked(self, button):
        # Gtk.main_quit()
        self.window.destroy()

    def display_results(self, results):
        self.display_tree(results)

    def remove_parents(self, button, parent):
        parent.destroy()

    def display_tree(self, results):
        print("prepairing tree")
        tree_store = Gtk.TreeStore(str, str)
        self.walk_tree(None, results, tree_store)
        self.results_tree.set_model(tree_store)

        column = Gtk.TreeViewColumn("Attribute")

        renderer = Gtk.CellRendererText()

        column.pack_start(renderer, True)
        column.add_attribute(renderer, "text", 0)

        self.results_tree.append_column(column)

        column2 = Gtk.TreeViewColumn("Value")

        hidden_renderer = Gtk.CellRendererText()
        hidden_renderer.set_visible(False)

        column2.pack_start(hidden_renderer, True)
        column2.add_attribute(hidden_renderer, "text", 1)
        column2.set_visible(False)

        self.results_tree.append_column(column2)

        select = self.results_tree.get_selection()
        select.connect("changed", self.on_tree_selection_changed)

    def display_value(self, value):
        val_view = Gtk.TextView()
        val_view.get_buffer().set_text(str(value))
        val_view.set_wrap_mode(Gtk.WrapMode.WORD)
        self.listbox_results.insert(val_view, -1)

    def on_tree_selection_changed(self, selection):
        model, treeiter = selection.get_selected()
        if treeiter is not None:
            print("You selected", model[treeiter][1])
            if isinstance(model[treeiter][1], dict) or isinstance(model[treeiter][1], list):
                pass
            else:
                for child in self.listbox_results.get_children():
                    child.destroy()
                self.display_value(model[treeiter][1])
        self.window.show_all()

    def walk_tree(self, parent, content, store):
        print(type(content))
        print(parent)
        if isinstance(content, dict):
            for key, value in content.items():
                print(type(value))
                print(str(key))
                if isinstance(value, dict) or isinstance(value, list):
                    tree_iter = store.append(parent, [str(key), ""])
                    self.walk_tree(tree_iter, value, store)
                else:
                    tree_iter = store.append(parent, [str(key), str(value)])
        elif isinstance(content, list):
            for item in content:
                if isinstance(item, dict):
                    print(item)
                    if "name" in item.keys():
                        tree_iter = store.append(parent, [str(item["name"]), ""])
                    elif "location" in item.keys():
                        tree_iter = store.append(parent, [str(item["location"]), ""])
                    else:
                        tree_iter = store.append(parent, [str(item.keys()[0]), ""])

                    self.walk_tree(tree_iter, item, store)
                elif isinstance(item, list):
                    self.walk_tree(tree_iter, item, store)
                elif parent is None and not isinstance(item, list) and not isinstance(item, dict):
                    tree_iter = store.append(parent, [str(item), str(item)])


class MainApp(Gtk.Application):
    def __init__(self, application_id, flags):
        Gtk.Application.__init__(self, application_id=application_id, flags=flags)
        self.connect("activate", self.new_window)

    def new_window(self, *args, **kwargs):
        AppMainWindow(self)


if __name__ == '__main__':
    print("Preparing launch …")
    app = MainApp("com.archyt.cephalon-6015-main", Gio.ApplicationFlags.FLAGS_NONE)
    app.run(sys.argv)
