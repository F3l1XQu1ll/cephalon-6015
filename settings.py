import sys
from typing import Any, Union

import gi
import cache
import search

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gio


class AppSettingsWindow(Gtk.Window):
    def __init__(self):
        try:
            self.GtkBuilder = Gtk.Builder.new_from_file("settings.ui")
            self.GtkBuilder.connect_signals(self)
        except GObject.GError:
            print("Error reading UI file")
            raise
        self.window = self.GtkBuilder.get_object("window")
        self.window.set_keep_above(True)

        view = self.GtkBuilder.get_object("viewport")

        notebook = Gtk.Notebook()
        notebook.set_scrollable(True)
        notebook.set_tab_pos(Gtk.PositionType.LEFT)
        notebook.popup_enable()
        view.add(notebook)
        for category in cache.categories:
            blacklist = search.get_key_blacklist(category)
            listbox = Gtk.ListBox()
            srolled_window = Gtk.ScrolledWindow()
            srolled_window.add(listbox)
            notebook.append_page(srolled_window, Gtk.Label(category.replace(".json", "")))
            for key in cache.load_keys(category):
                listbox_row = Gtk.ListBoxRow()
                grid = Gtk.Grid()
                listbox_row.add(grid)

                key_label = Gtk.Label(str(key))
                key_check = Gtk.CheckButton()
                if key in blacklist:
                    key_check.set_active(False)
                else:
                    key_check.set_active(True)
                key_check.connect("toggled", self.on_toggle, key, category)
                grid.add(key_check)
                grid.add(key_label)
                listbox.add(listbox_row)

        self.window.show_all()

    def on_button_quit_clicked(self, button):
        # Gtk.main_quit()
        self.window.destroy()

    def on_toggle(self, button, key, category):
        print(f"toggled button for key: {key}")
        blacklist: list = search.get_key_blacklist(category)
        if key in blacklist and button.get_active() is True:
            blacklist.remove(key)
        elif key not in blacklist and button.get_active() is False:
            blacklist.append(key)

        search.set_key_blacklist(category, blacklist)
