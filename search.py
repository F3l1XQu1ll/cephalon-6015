import pickle
import cache
import os
import apirequests

categories = cache.categories


def get_type(query):
    for category in categories:
        index_filename = ('index_data' + os.sep + category).replace(".json", "")
        with open(index_filename, "rb") as index_file:
            items = pickle.load(index_file)
            for item in items:
                if item.lower() == query.lower():
                    return category.replace(".json", "")
    return None


def do_advanced_search(item_type, query):
    data_index_filename = ('index_data' + os.sep + 'data' + os.sep + item_type)

    if " " in query:
        requested_item = query.split(" ")[0]
        requested_item_sub = query.split(" ")[1]
    else:
        requested_item = query
        requested_item_sub = None

    with open(data_index_filename, "rb") as index_file:
        items = pickle.load(index_file)
        # print(items)
        for item in items:
            if item["name"].lower() == requested_item:
                if requested_item_sub is not None and requested_item_sub in item.keys():
                    print(f"{item_type} -> {requested_item} -> {requested_item_sub}: {item[requested_item_sub]}")
                    return item[requested_item_sub]
                else:
                    print(f"no valid sub-catagory given … returning raw-data")
                    return item
            else:
                continue
    print(f"Somehow, lookup of requested entry {requested_item} in category {item_type} failed! Giving up :(")
    return None


def do_search(query):
    if " " in query:
        item_type = get_type(query.split(" ")[0])
    else:
        item_type = get_type(query)
    result = None
    if item_type is not None:
        blacklist = get_key_blacklist(item_type)
        print(f"Found query \"{query}\" in category {item_type}")
        # apirequests.search_request(item_type, query.lower())
        result = do_advanced_search(item_type, query.lower())
        if isinstance(result, dict):
            for key in blacklist:
                if key in result.keys():
                    result.pop(key)
        print(type(result))
        print(f"Result of search was {result}")
        # return "Wtf lol"
        # return result

    # print(f"Could not find requested item: {query}")
    return result


def get_key_blacklist(category):
    data_index_filename = ('index_data' + os.sep + 'blacklists' + os.sep + category.replace(".json", ""))
    try:
        with open(data_index_filename, "rb") as index_file:
            items = pickle.load(index_file)
    except FileNotFoundError:
        items = []
    return items


def set_key_blacklist(category, blacklist):
    data_index_filename = ('index_data' + os.sep + 'blacklists' + os.sep + category.replace(".json", ""))

    try:
        with open(data_index_filename, "wb") as index_file:
            pickle.dump(blacklist, index_file)
    except FileNotFoundError:
        os.makedirs(os.path.dirname(data_index_filename))
        set_key_blacklist(category, blacklist)


def get_completitions(query):
    print(query)
    only_names = False
    if " " in query:
        item_type = get_type(query.split(" ")[0])
        requested_item = query.split(" ")[0]
        only_names = False
    else:
        item_type = get_type(query)
        requested_item = query
        only_names = True

    print(requested_item)
    print(only_names)

    if item_type is not None:
        blacklist = get_key_blacklist(item_type)
        data_index_filename = ('index_data' + os.sep + 'data' + os.sep + item_type)
        with open(data_index_filename, "rb") as index_file:
            items = pickle.load(index_file)
            item_names_list = []
            for item in items:
                # print(item["name"])
                if not only_names:
                    print(requested_item.lower())
                    if item["name"].lower() == requested_item.lower():
                        print("names equal")
                        # print(item.keys())
                        keys_list = []
                        for key in item.keys():
                            if key not in blacklist:
                                keys_list.append(item["name"] + " " + key)
                        return keys_list
                    else:
                        continue
                else:
                    item_names_list.append(item["name"])
            return item_names_list
    return None
