import requests
import resultview


def request(request):
    r = requests.get(request)
    print(f"Status of request was: {r.status_code}")
    return r


def request_all():
    request_str = "https://api.warframestat.us/pc"
    r = request(request_str)
    view = resultview.ResultView(r)
    view.set_keep_above(True)
    view.show_all()


def warframe_data(warframe_name):
    request_str = "https://api.warframestat.us/warframes/search/" + warframe_name
    request(request_str)


@DeprecationWarning
def search_request(item_type, query):
    key = None
    """
    :type query: str
    """
    print(item_type.lower())
    if " " in query:
        key = query.split(" ")[1]
        print(key)
    request_str = f"https://api.warframestat.us/warframes/search/" + query.split(" ")[0]
    r = request(request_str)
