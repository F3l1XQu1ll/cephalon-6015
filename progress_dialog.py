import sys
import threading
from typing import Any, Union

import gi
import cache
import search

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, Gio, GLib


class ProgressDialogWindow(Gtk.Window):
    def __init__(self, message: str, max_progress: int):
        self.window = Gtk.Window("GTK_WINDOW_POPUP")
        self.window.set_keep_above(True)
        self.window.set_position(Gtk.WindowPosition.CENTER)
        self.progress_bar = Gtk.ProgressBar()
        self.progress_bar.set_text(message)
        self.progress_bar.set_show_text(message)
        self.max_progress = max_progress

        if max_progress == 0:
            self.progress_bar.pulse()
        else:
            self.progress_bar.set_fraction(0.0)

        self.window.add(self.progress_bar)
        self.window.show_all()

    def update_progress(self, fraction: float):
        self.progress_bar.set_fraction(fraction)

    def close(self):
        self.window.destroy()
