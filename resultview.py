'''
Created on 06.05.2020

@author: felixq
'''

from collections import namedtuple
import json
import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

tree_store = Gtk.TreeStore(str)


class ResultView(Gtk.Window):
    '''
    classdocs
    '''

    def _json_object_hook(self, d):
        return namedtuple('X', d.keys())(*d.values())

    def json2obj(self, data):
        return json.loads(data, object_hook=self._json_object_hook)

    def iterate_json(self, dictionary):
        for key, value in dictionary.items():
            if isinstance(value, dict):
                self.iterate_json(value)
            else:
                print([str(value)])
                tree_store.append(None, [str(value)])

    def __init__(self, r):
        '''
        Constructor
        '''
        print(type(r))
        print(type(r.json()[0]))
        r = r.json()[0]
        print(r)

        Gtk.Window.__init__(self, title="Response View")
        grid = Gtk.Grid()
        self.add(grid)

        label = Gtk.Label()
        label.set_text("Text")
        grid.add(label)

        tree_view = Gtk.TreeView()
        tree_column = Gtk.TreeViewColumn('Info')
        tree_view.append_column(tree_column)

        cell = Gtk.CellRendererText()
        tree_column.pack_start(cell, True)
        tree_column.add_attribute(cell, 'text', 0)

#        self.iterate_json(r)
        tree_store.append(None, ['test'])
        tree_store.append(None, ['test2'])
        tree_store.append(None, ['test142429821982lkanskna wd oiwhda ioöwah dioahd k'])
        tree_store.append(None, ['http://warframe.fandom.com/wiki/Garuda'])
        tree_store.append(None, ['"http://warframe.fandom.com/wiki/Garuda"'])
        tree_store.append(None, [str(124)])

        tree_view.set_model(tree_store)

        grid.add(tree_view)
